-- phpMyAdmin SQL Dump
-- version 4.9.6
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Мар 06 2021 г., 11:15
-- Версия сервера: 8.0.21-0ubuntu0.20.04.4
-- Версия PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `Frequent_Flyer_Program`
--

-- --------------------------------------------------------

--
-- Структура таблицы `awarded_flights`
--

CREATE TABLE `awarded_flights` (
                                   `id_route` int NOT NULL COMMENT 'ID маршрута',
                                   `id_passenger` int NOT NULL COMMENT 'ID пассажира',
                                   `points_awarded` int NOT NULL COMMENT 'Баллов получено',
                                   `date_and_time` datetime NOT NULL COMMENT 'Дата и время'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `awarded_flights`
--

INSERT INTO `awarded_flights` (`id_route`, `id_passenger`, `points_awarded`, `date_and_time`) VALUES
(1, 4, 1331, '2019-09-10 16:35:00'),
(3, 4, 4500, '2019-09-11 06:30:00'),
(17, 4, 1997, '2019-09-17 14:05:00'),
(20, 4, 3994, '2019-10-03 01:50:00'),
(2, 4, 1664, '2019-10-09 03:55:00'),
(8, 4, 1801, '2020-10-09 13:00:00'),
(17, 8, 1997, '2020-09-17 15:55:24'),
(18, 15, 3033, '2020-01-18 14:44:04'),
(20, 1, 3994, '2019-10-14 05:00:58'),
(3, 16, 4500, '2020-01-08 20:57:00'),
(2, 9, 1664, '2018-12-27 00:06:12'),
(2, 3, 1664, '2018-12-27 00:06:12'),
(2, 11, 1664, '2018-12-27 00:06:12'),
(21, 8, 1113, '2019-10-17 07:37:16'),
(21, 8, 1113, '2020-05-03 16:59:33'),
(22, 5, 1109, '2020-08-17 16:59:42'),
(23, 17, 1517, '2018-01-23 00:53:23'),
(12, 14, 1619, '2019-05-23 00:45:44'),
(5, 7, 1399, '2020-04-23 23:32:37'),
(10, 2, 500, '2020-03-15 06:38:54'),
(21, 6, 1113, '2019-08-24 10:49:16'),
(2, 3, 1664, '2020-03-07 12:17:30'),
(23, 2, 1517, '2020-10-19 14:15:39'),
(23, 3, 1517, '2020-10-19 14:15:39'),
(24, 12, 1517, '2019-08-31 14:00:56');

-- --------------------------------------------------------

--
-- Структура таблицы `passenger`
--

CREATE TABLE `passenger` (
                             `id` int NOT NULL COMMENT 'ID пассажира',
                             `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ФИО'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `passenger`
--

INSERT INTO `passenger` (`id`, `name`) VALUES
(1, 'Бирюков Марк Константинович'),
(2, 'Владимирова София Богдановна'),
(3, 'Гусев Никита Богданович'),
(4, 'Рубцов Иван'),
(5, 'Козлов Михаил Сергеевич'),
(6, 'Корнеева Мария Романовна'),
(7, 'Лебедева София Тимуровна'),
(8, 'Леонова Мария Романовна'),
(9, 'Попов Михаил Михайлович'),
(10, 'Куликова Любовь'),
(11, 'Свешников Евгений'),
(12, 'Шевцов Валерий Львович'),
(13, 'Родина Елизавета Михайловна'),
(14, 'Михайлов Матвей Елисеевич'),
(15, 'Семенова Дарья Владимировна'),
(16, 'Трофимов Макар Александрович'),
(17, 'Соколова Валерия Романовна');

-- --------------------------------------------------------

--
-- Структура таблицы `redeemed_flights`
--

CREATE TABLE `redeemed_flights` (
                                    `id_route` int NOT NULL COMMENT 'ID маршрута',
                                    `id_passenger` int NOT NULL COMMENT 'ID пассажира',
                                    `points_redeemed` int NOT NULL COMMENT 'Баллов потрачено',
                                    `date_and_time` datetime NOT NULL COMMENT 'Дата и время'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `redeemed_flights`
--

INSERT INTO `redeemed_flights` (`id_route`, `id_passenger`, `points_redeemed`, `date_and_time`) VALUES
(7, 4, 12500, '2020-08-17 08:35:00'),
(21, 2, 17500, '2020-08-01 20:37:00'),
(21, 1, 17500, '2020-08-01 20:37:00'),
(22, 2, 17500, '2020-08-17 08:45:00'),
(22, 1, 17500, '2020-08-17 08:45:00'),
(10, 13, 10000, '2020-09-30 07:05:00'),
(5, 4, 12500, '2020-11-25 23:31:10'),
(1, 1, 12500, '2019-12-01 09:31:09'),
(1, 11, 12500, '2019-12-01 09:31:09'),
(16, 15, 12500, '2019-01-30 06:32:50'),
(24, 15, 12500, '2018-06-09 13:46:49'),
(7, 15, 12500, '2020-03-27 17:45:12'),
(11, 3, 12500, '2020-11-29 09:17:43'),
(16, 16, 12500, '2018-04-25 23:07:12'),
(18, 12, 25000, '2019-11-30 03:34:28');

-- --------------------------------------------------------

--
-- Структура таблицы `route`
--

CREATE TABLE `route` (
                         `id` int NOT NULL COMMENT 'ID маршрута',
                         `from_to` varchar(255) NOT NULL COMMENT 'Направление (город-город)',
                         `award_in_points` int NOT NULL COMMENT 'Премия в баллах',
                         `cost_in_points` int NOT NULL COMMENT 'Стоимость в баллах'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `route`
--

INSERT INTO `route` (`id`, `from_to`, `award_in_points`, `cost_in_points`) VALUES
(1, 'Сургут - Москва', 1331, 12500),
(2, 'Москва - Сургут', 1664, 12500),
(3, 'Москва - Лондон', 4500, 25000),
(4, 'Лондон - Москва', 4500, 25000),
(5, 'Санкт-Петербург  - Екатеринбург', 1399, 12500),
(6, 'Екатеринбург - Санкт-Петербург ', 1119, 12500),
(7, 'Сургут - Санкт-Петербург ', 1441, 12500),
(8, 'Санкт-Петербург - Сургут', 1801, 12500),
(9, 'Санкт-Петербург - Москва', 500, 10000),
(10, 'Москва - Санкт-Петербург ', 500, 10000),
(11, 'Москва - Екатеринбург', 1974, 12500),
(12, 'Екатеринбург - Москва', 1619, 12500),
(13, 'Москва - Мадрид', 2030, 30000),
(14, 'Мадрид - Москва', 3291, 30000),
(15, 'Лондон - Мадрид', 3033, 12500),
(16, 'Мадрид - Лондон', 3033, 12500),
(17, 'Лондон - Торонто', 1997, 25000),
(18, 'Торонто - Лондон', 1997, 25000),
(19, 'Москва - Торонто', 3681, 45000),
(20, 'Торонто - Москва', 3994, 45000),
(21, 'Сургут - Анталья', 1113, 17500),
(22, 'Анталья - Сургут', 1109, 17500),
(23, 'Санкт-Петербург - Осло', 1517, 12500),
(24, 'Осло - Санкт-Петербург', 1517, 12500);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `awarded_flights`
--
ALTER TABLE `awarded_flights`
    ADD KEY `id_route` (`id_route`),
  ADD KEY `id_passenger` (`id_passenger`);

--
-- Индексы таблицы `passenger`
--
ALTER TABLE `passenger`
    ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `redeemed_flights`
--
ALTER TABLE `redeemed_flights`
    ADD KEY `id_route` (`id_route`),
  ADD KEY `id_passenger` (`id_passenger`);

--
-- Индексы таблицы `route`
--
ALTER TABLE `route`
    ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `passenger`
--
ALTER TABLE `passenger`
    MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'ID пассажира', AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT для таблицы `route`
--
ALTER TABLE `route`
    MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'ID маршрута', AUTO_INCREMENT=25;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `awarded_flights`
--
ALTER TABLE `awarded_flights`
    ADD CONSTRAINT `awarded_flights_ibfk_1` FOREIGN KEY (`id_route`) REFERENCES `route` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `awarded_flights_ibfk_2` FOREIGN KEY (`id_passenger`) REFERENCES `passenger` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `redeemed_flights`
--
ALTER TABLE `redeemed_flights`
    ADD CONSTRAINT `redeemed_flights_ibfk_1` FOREIGN KEY (`id_route`) REFERENCES `route` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `redeemed_flights_ibfk_2` FOREIGN KEY (`id_passenger`) REFERENCES `passenger` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
