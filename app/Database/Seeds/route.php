<?php namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class route extends Seeder
{
    public function run()
    {
        //routes
    /*
        $data = [
            'id' => '',
            'from_to'=> '',
            'award_in_points' => '',
            'cost_in_points' => '',
        ];
        $this->db->table('route')->insert($data);
    */
        $data = [
            'id' => '1',
            'from_to'=> 'Сургут - Москва',
            'award_in_points' => '1331',
            'cost_in_points' => '12500',
        ];
        $this->db->table('route')->insert($data);

        $data = [
            'id' => '2',
            'from_to'=> 'Москва - Сургут',
            'award_in_points' => '1664',
            'cost_in_points' => '12500',
        ];
        $this->db->table('route')->insert($data);

        $data = [
            'id' => '3',
            'from_to'=> 'Москва - Лондон',
            'award_in_points' => '4500',
            'cost_in_points' => '25000',
        ];
        $this->db->table('route')->insert($data);

        $data = [
            'id' => '4',
            'from_to'=> 'Лондон - Москва',
            'award_in_points' => '4500',
            'cost_in_points' => '25000',
        ];
        $this->db->table('route')->insert($data);

        $data = [
            'id' => '5',
            'from_to'=> 'Санкт-Петербург - Екатеринбург',
            'award_in_points' => '1399',
            'cost_in_points' => '12500',
        ];
        $this->db->table('route')->insert($data);

        $data = [
            'id' => '6',
            'from_to'=> 'Екатеринбург - Санкт-Петербург',
            'award_in_points' => '1119',
            'cost_in_points' => '12500',
        ];
        $this->db->table('route')->insert($data);

        $data = [
            'id' => '7',
            'from_to'=> 'Сургут - Санкт-Петербург',
            'award_in_points' => '1441',
            'cost_in_points' => '12500',
        ];
        $this->db->table('route')->insert($data);

        $data = [
            'id' => '8',
            'from_to'=> 'Санкт-Петербург - Сургут',
            'award_in_points' => '1801',
            'cost_in_points' => '12500',
        ];
        $this->db->table('route')->insert($data);

        $data = [
            'id' => '9',
            'from_to'=> 'Санкт-Петербург - Москва',
            'award_in_points' => '500',
            'cost_in_points' => '10000',
        ];
        $this->db->table('route')->insert($data);

        $data = [
            'id' => '10',
            'from_to'=> 'Москва - Санкт-Петербург',
            'award_in_points' => '500',
            'cost_in_points' => '10000',
        ];
        $this->db->table('route')->insert($data);

        $data = [
            'id' => '11',
            'from_to'=> 'Москва - Екатеринбург',
            'award_in_points' => '1974',
            'cost_in_points' => '12500',
        ];
        $this->db->table('route')->insert($data);

        $data = [
            'id' => '12',
            'from_to'=> 'Екатеринбург - Москва',
            'award_in_points' => '1619',
            'cost_in_points' => '12500',
        ];
        $this->db->table('route')->insert($data);

        $data = [
            'id' => '13',
            'from_to'=> 'Москва - Мадрид',
            'award_in_points' => '2030',
            'cost_in_points' => '30000',
        ];
        $this->db->table('route')->insert($data);

        $data = [
            'id' => '14',
            'from_to'=> 'Мадрид - Москва',
            'award_in_points' => '3291',
            'cost_in_points' => '30000',
        ];
        $this->db->table('route')->insert($data);

        $data = [
            'id' => '15',
            'from_to'=> 'Лондон - Мадрид',
            'award_in_points' => '3033',
            'cost_in_points' => '12500',
        ];
        $this->db->table('route')->insert($data);

        $data = [
            'id' => '16',
            'from_to'=> 'Мадрид - Лондон',
            'award_in_points' => '3033',
            'cost_in_points' => '12500',
        ];
        $this->db->table('route')->insert($data);

        $data = [
            'id' => '17',
            'from_to'=> 'Лондон - Торонто',
            'award_in_points' => '1997',
            'cost_in_points' => '25000',
        ];
        $this->db->table('route')->insert($data);

        $data = [
            'id' => '18',
            'from_to'=> 'Торонто - Лондон',
            'award_in_points' => '1997',
            'cost_in_points' => '25000',
        ];
        $this->db->table('route')->insert($data);

        $data = [
            'id' => '19',
            'from_to'=> 'Москва - Торонто',
            'award_in_points' => '3681',
            'cost_in_points' => '45000',
        ];
        $this->db->table('route')->insert($data);

        $data = [
            'id' => '20',
            'from_to'=> 'Торонто - Москва',
            'award_in_points' => '3994',
            'cost_in_points' => '45000',
        ];
        $this->db->table('route')->insert($data);

        $data = [
            'id' => '21',
            'from_to'=> 'Сургут - Анталья',
            'award_in_points' => '1113',
            'cost_in_points' => '17500',
        ];
        $this->db->table('route')->insert($data);

        $data = [
            'id' => '22',
            'from_to'=> 'Анталья - Сургут',
            'award_in_points' => '1109',
            'cost_in_points' => '17500',
        ];
        $this->db->table('route')->insert($data);

        $data = [
            'id' => '23',
            'from_to'=> 'Санкт-Петербург - Осло',
            'award_in_points' => '1517',
            'cost_in_points' => '12500',
        ];
        $this->db->table('route')->insert($data);

        $data = [
            'id' => '24',
            'from_to'=> 'Осло - Санкт-Петербург',
            'award_in_points' => '1517',
            'cost_in_points' => '12500',
        ];
        $this->db->table('route')->insert($data);


        //passengers
    /*
        $data = [
            'id'=> '',
            'name'=> '',
        ];
        $this->db->table('passenger')->insert($data);
    */
        $data = [
            'id'=> '1',
            'name'=> 'Бирюков Марк Константинович',
        ];
        $this->db->table('passenger')->insert($data);

        $data = [
            'id'=> '2',
            'name'=> 'Владимирова София Богдановна',
        ];
        $this->db->table('passenger')->insert($data);

        $data = [
            'id'=> '3',
            'name'=> 'Гусев Никита Богданович',
        ];
        $this->db->table('passenger')->insert($data);

        $data = [
            'id'=> '4',
            'name'=> 'Рубцов Иван',
        ];
        $this->db->table('passenger')->insert($data);

        $data = [
            'id'=> '5',
            'name'=> 'Козлов Михаил Сергеевич',
        ];
        $this->db->table('passenger')->insert($data);

        $data = [
            'id'=> '6',
            'name'=> 'Корнеева Мария Романовна',
        ];
        $this->db->table('passenger')->insert($data);

        $data = [
            'id'=> '7',
            'name'=> 'Лебедева София Тимуровна',
        ];
        $this->db->table('passenger')->insert($data);

        $data = [
            'id'=> '8',
            'name'=> 'Леонова Мария Романовна',
        ];
        $this->db->table('passenger')->insert($data);

        $data = [
            'id'=> '9',
            'name'=> 'Попов Михаил Михайлович',
        ];
        $this->db->table('passenger')->insert($data);

        $data = [
            'id'=> '10',
            'name'=> 'Куликова Любовь',
        ];
        $this->db->table('passenger')->insert($data);

        $data = [
            'id'=> '11',
            'name'=> 'Свешников Евгений',
        ];
        $this->db->table('passenger')->insert($data);

        $data = [
            'id'=> '12',
            'name'=> 'Шевцов Валерий Львович',
        ];
        $this->db->table('passenger')->insert($data);

        $data = [
            'id'=> '13',
            'name'=> 'Родина Елизавета Михайловна',
        ];
        $this->db->table('passenger')->insert($data);

        $data = [
            'id'=> '14',
            'name'=> 'Михайлов Матвей Елисеевич',
        ];
        $this->db->table('passenger')->insert($data);

        $data = [
            'id'=> '15',
            'name'=> 'Семенова Дарья Владимировна',
        ];
        $this->db->table('passenger')->insert($data);

        $data = [
            'id'=> '16',
            'name'=> 'Трофимов Макар Александрович',
        ];
        $this->db->table('passenger')->insert($data);

        $data = [
            'id'=> '17',
            'name'=> 'Соколова Валерия Романовна',
        ];
        $this->db->table('passenger')->insert($data);


        //awarded flights
    /*
        $data = [
            'id_route'=> '',
            'id_passenger' => '',
            'points_awarded' => '',
            'date_and_time' => '',
        ];
        $this->db->table('awarded_flights')->insert($data);
    */
        $data = [
            'id_route'=> '1',
            'id_passenger' => '4',
            'points_awarded' => '1331',
            'date_and_time' => '2019-09-10 16:35:00',
        ];
        $this->db->table('awarded_flights')->insert($data);

        $data = [
            'id_route'=> '3',
            'id_passenger' => '4',
            'points_awarded' => '4500',
            'date_and_time' => '2019-09-11 06:30:00',
        ];
        $this->db->table('awarded_flights')->insert($data);

        $data = [
            'id_route'=> '17',
            'id_passenger' => '4',
            'points_awarded' => '1997',
            'date_and_time' => '2019-09-17 14:05:00',
        ];
        $this->db->table('awarded_flights')->insert($data);

        $data = [
            'id_route'=> '20',
            'id_passenger' => '4',
            'points_awarded' => '3994',
            'date_and_time' => '2019-10-03 01:50:00',
        ];
        $this->db->table('awarded_flights')->insert($data);

        $data = [
            'id_route'=> '2',
            'id_passenger' => '4',
            'points_awarded' => '1664',
            'date_and_time' => '2019-10-09 03:55:00',
        ];
        $this->db->table('awarded_flights')->insert($data);

        $data = [
            'id_route'=> '8',
            'id_passenger' => '4',
            'points_awarded' => '1801',
            'date_and_time' => '2020-10-09 13:00:00',
        ];
        $this->db->table('awarded_flights')->insert($data);

        $data = [
            'id_route'=> '17',
            'id_passenger' => '8',
            'points_awarded' => '1997',
            'date_and_time' => '2020-09-17 15:55:24',
        ];
        $this->db->table('awarded_flights')->insert($data);

        $data = [
            'id_route'=> '18',
            'id_passenger' => '15',
            'points_awarded' => '3033',
            'date_and_time' => '2020-01-18 14:44:04',
        ];
        $this->db->table('awarded_flights')->insert($data);

        $data = [
            'id_route'=> '20',
            'id_passenger' => '1',
            'points_awarded' => '3994',
            'date_and_time' => '2019-10-14 05:00:58',
        ];
        $this->db->table('awarded_flights')->insert($data);

        $data = [
            'id_route'=> '3',
            'id_passenger' => '16',
            'points_awarded' => '4500',
            'date_and_time' => '2020-01-08 20:57:00',
        ];
        $this->db->table('awarded_flights')->insert($data);

        $data = [
            'id_route'=> '2',
            'id_passenger' => '9',
            'points_awarded' => '1664',
            'date_and_time' => '2018-12-27 00:06:12',
        ];
        $this->db->table('awarded_flights')->insert($data);

        $data = [
            'id_route'=> '2',
            'id_passenger' => '3',
            'points_awarded' => '1664',
            'date_and_time' => '2018-12-27 00:06:12',
        ];
        $this->db->table('awarded_flights')->insert($data);

        $data = [
            'id_route'=> '2',
            'id_passenger' => '11',
            'points_awarded' => '1664',
            'date_and_time' => '2018-12-27 00:06:12',
        ];
        $this->db->table('awarded_flights')->insert($data);

        $data = [
            'id_route'=> '21',
            'id_passenger' => '8',
            'points_awarded' => '1113',
            'date_and_time' => '2019-10-17 07:37:16',
        ];
        $this->db->table('awarded_flights')->insert($data);

        $data = [
            'id_route'=> '21',
            'id_passenger' => '8',
            'points_awarded' => '1113',
            'date_and_time' => '2020-05-03 16:59:33',
        ];
        $this->db->table('awarded_flights')->insert($data);

        $data = [
            'id_route'=> '22',
            'id_passenger' => '5',
            'points_awarded' => '1109',
            'date_and_time' => '2020-08-17 16:59:42',
        ];
        $this->db->table('awarded_flights')->insert($data);

        $data = [
            'id_route'=> '23',
            'id_passenger' => '17',
            'points_awarded' => '1517',
            'date_and_time' => '2018-01-23 00:53:23',
        ];
        $this->db->table('awarded_flights')->insert($data);

        $data = [
            'id_route'=> '12',
            'id_passenger' => '14',
            'points_awarded' => '1619',
            'date_and_time' => '2019-05-23 00:45:44',
        ];
        $this->db->table('awarded_flights')->insert($data);

        $data = [
            'id_route'=> '5',
            'id_passenger' => '7',
            'points_awarded' => '1399',
            'date_and_time' => '2020-04-23 23:32:37',
        ];
        $this->db->table('awarded_flights')->insert($data);

        $data = [
            'id_route'=> '10',
            'id_passenger' => '2',
            'points_awarded' => '500',
            'date_and_time' => '2020-03-15 06:38:54',
        ];
        $this->db->table('awarded_flights')->insert($data);

        $data = [
            'id_route'=> '21',
            'id_passenger' => '6',
            'points_awarded' => '1113',
            'date_and_time' => '2019-08-24 10:49:16',
        ];
        $this->db->table('awarded_flights')->insert($data);

        $data = [
            'id_route'=> '2',
            'id_passenger' => '3',
            'points_awarded' => '1664',
            'date_and_time' => '2020-03-07 12:17:30',
        ];
        $this->db->table('awarded_flights')->insert($data);

        $data = [
            'id_route'=> '23',
            'id_passenger' => '2',
            'points_awarded' => '1517',
            'date_and_time' => '2020-10-19 14:15:39',
        ];
        $this->db->table('awarded_flights')->insert($data);

        $data = [
            'id_route'=> '23',
            'id_passenger' => '3',
            'points_awarded' => '1517',
            'date_and_time' => '2020-10-19 14:15:39',
        ];
        $this->db->table('awarded_flights')->insert($data);

        $data = [
            'id_route'=> '24',
            'id_passenger' => '12',
            'points_awarded' => '1517',
            'date_and_time' => '2019-08-31 14:00:56',
        ];
        $this->db->table('awarded_flights')->insert($data);


        //redeemed flights
    /*
        $data = [
            'id_route'=> '',
            'id_passenger' => '',
            'points_redeemed' => '',
            'date_and_time' => '',
        ];
        $this->db->table('redeemed_flights')->insert($data);
    */
        $data = [
            'id_route'=> '7',
            'id_passenger' => '4',
            'points_redeemed' => '12500',
            'date_and_time' => '2020-08-17 08:35:00',
        ];
        $this->db->table('redeemed_flights')->insert($data);

        $data = [
            'id_route'=> '21',
            'id_passenger' => '2',
            'points_redeemed' => '17500',
            'date_and_time' => '2020-08-01 20:37:00',
        ];
        $this->db->table('redeemed_flights')->insert($data);

        $data = [
            'id_route'=> '21',
            'id_passenger' => '1',
            'points_redeemed' => '17500',
            'date_and_time' => '2020-08-01 20:37:00',
        ];
        $this->db->table('redeemed_flights')->insert($data);

        $data = [
            'id_route'=> '22',
            'id_passenger' => '2',
            'points_redeemed' => '17500',
            'date_and_time' => '2020-08-17 08:45:00',
        ];
        $this->db->table('redeemed_flights')->insert($data);

        $data = [
            'id_route'=> '22',
            'id_passenger' => '1',
            'points_redeemed' => '17500',
            'date_and_time' => '2020-08-17 08:45:00',
        ];
        $this->db->table('redeemed_flights')->insert($data);

        $data = [
            'id_route'=> '10',
            'id_passenger' => '13',
            'points_redeemed' => '10000',
            'date_and_time' => '2020-09-30 07:05:00',
        ];
        $this->db->table('redeemed_flights')->insert($data);

        $data = [
            'id_route'=> '5',
            'id_passenger' => '4',
            'points_redeemed' => '12500',
            'date_and_time' => '2020-11-25 23:31:10',
        ];
        $this->db->table('redeemed_flights')->insert($data);

        $data = [
            'id_route'=> '1',
            'id_passenger' => '1',
            'points_redeemed' => '12500',
            'date_and_time' => '2019-12-01 09:31:09',
        ];
        $this->db->table('redeemed_flights')->insert($data);

        $data = [
            'id_route'=> '1',
            'id_passenger' => '11',
            'points_redeemed' => '12500',
            'date_and_time' => '2019-12-01 09:31:09',
        ];
        $this->db->table('redeemed_flights')->insert($data);

        $data = [
            'id_route'=> '16',
            'id_passenger' => '15',
            'points_redeemed' => '12500',
            'date_and_time' => '2019-01-30 06:32:50',
        ];
        $this->db->table('redeemed_flights')->insert($data);

        $data = [
            'id_route'=> '24',
            'id_passenger' => '15',
            'points_redeemed' => '12500',
            'date_and_time' => '2018-06-09 13:46:49',
        ];
        $this->db->table('redeemed_flights')->insert($data);

        $data = [
            'id_route'=> '7',
            'id_passenger' => '15',
            'points_redeemed' => '12500',
            'date_and_time' => '2020-03-27 17:45:12',
        ];
        $this->db->table('redeemed_flights')->insert($data);

        $data = [
            'id_route'=> '11',
            'id_passenger' => '3',
            'points_redeemed' => '12500',
            'date_and_time' => '2020-11-29 09:17:43',
        ];
        $this->db->table('redeemed_flights')->insert($data);

        $data = [
            'id_route'=> '16',
            'id_passenger' => '16',
            'points_redeemed' => '12500',
            'date_and_time' => '2018-04-25 23:07:12',
        ];
        $this->db->table('redeemed_flights')->insert($data);

        $data = [
            'id_route'=> '18',
            'id_passenger' => '12',
            'points_redeemed' => '25000',
            'date_and_time' => '2019-11-30 03:34:28',
        ];
        $this->db->table('redeemed_flights')->insert($data);
    }
}