<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Route extends Migration
{
	public function up()
	{
		//route
        if (!$this->db->tableexists('route'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);
            // Build Schema
            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'from_to' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
                'award_in_points' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
                'cost_in_points' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE)
            ));
            // Create table
            $this->forge->createtable('route', TRUE);
        }

        //passenger
        if (!$this->db->tableexists('passenger'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);
            // Build Schema
            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'name' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE)
            ));
            // Create table
            $this->forge->createtable('passenger', TRUE);
        }

        //awarded_flights
        if (!$this->db->tableexists('awarded_flights'))
        {
            // Build Schema
            $this->forge->addfield(array(
                'id_route' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
                'id_passenger' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
                'points_awarded' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
                'date_and_time' => array('type' => 'DATETIME', 'null' => FALSE)
            ));
            //Setup foreign keys
            $this->forge->addForeignKey('id_route','route','id','RESTRICT','RESTRICT');
            $this->forge->addForeignKey('id_passenger','passenger','id','RESTRICT','RESTRICT');
            // Create table
            $this->forge->createtable('awarded_flights', TRUE);
        }

        //redeemed_flights
        if (!$this->db->tableexists('redeemed_flights'))
        {
            // Build Schema
            $this->forge->addfield(array(
                'id_route' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
                'id_passenger' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
                'points_redeemed' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
                'date_and_time' => array('type' => 'DATETIME', 'null' => FALSE)
            ));
            //Setup foreign keys
            $this->forge->addForeignKey('id_route','route','id','RESTRICT','RESTRICT');
            $this->forge->addForeignKey('id_passenger','passenger','id','RESTRICT','RESTRICT');
            // Create table
            $this->forge->createtable('redeemed_flights', TRUE);
        }
	}

	public function down()
	{
        $this->forge->dropTable('route');
        $this->forge->dropTable('passenger');
        $this->forge->dropTable('awarded_flights');
        $this->forge->dropTable('redeemed_flights');
	}
}
