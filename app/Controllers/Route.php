<?php namespace App\Controllers;

use App\Models\RouteModel;

class Route extends BaseController

{
    public function index() //Обображение всех записей
    {
        //проверка аутентицикации пользователя
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new RouteModel();
        $data ['route'] = $model->getRoute();
        echo view('route/view_all', $this->withIon($data));
    }

    public function view($id = null) //отображение одной записи
    {
        //проверка аутентицикации пользователя
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new RouteModel();
        $data ['route'] = $model->getRoute($id);
        echo view('route/view', $this->withIon($data));
    }

    public function create()
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        helper(['form']);
        $data ['validation'] = \Config\Services::validation();
        echo view('route/create', $this->withIon($data));
    }

    public function store()
    {
        helper(['form','url']);

        if ($this->request->getMethod() === 'post' && $this->validate([
                'from_to' => 'required|min_length[5]|max_length[255]|regex_match[/^[A-ZА-ЯЁ]([a-zа-яё])+(-[A-ZА-ЯЁ]?([a-zа-яё])+)*\s-\s[A-ZА-ЯЁ]([a-zа-яё])+(-[A-ZА-ЯЁ]?([a-zа-яё])+)*$/u]|is_unique[route.from_to]',
                'award_in_points'  => 'required|numeric',
                'cost_in_points'  => 'required|numeric',
            ]))
        {
            $model = new RouteModel();
            $model->save([
                'from_to' => $this->request->getPost('from_to'),
                'award_in_points' => $this->request->getPost('award_in_points'),
                'cost_in_points' => $this->request->getPost('cost_in_points'),
            ]);
            session()->setFlashdata('message', lang('Airline.route_create_success'));
            return redirect()->to('/route');
        }
        else
        {
            return redirect()->to('/route/create')->withInput();
        }
    }

    public function edit($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new RouteModel();

        helper(['form']);
        $data ['route'] = $model->getRoute($id);
        $data ['validation'] = \Config\Services::validation();
        echo view('route/edit', $this->withIon($data));
    }

    public function update()
    {
        helper(['form','url']);
        echo '/route/edit/'.$this->request->getPost('id');
        if ($this->request->getMethod() === 'post' && $this->validate([
                'id'  => 'required',
                'from_to' => 'required|min_length[3]|max_length[255]|regex_match[/^[A-ZА-ЯЁ]([a-zа-яё])+(-[A-ZА-ЯЁ]?([a-zа-яё])+)*\s-\s[A-ZА-ЯЁ]([a-zа-яё])+(-[A-ZА-ЯЁ]?([a-zа-яё])+)*$/u]',
                'award_in_points'  => 'required|numeric',
                'cost_in_points'  => 'required|numeric',
            ]))
        {
            $model = new RouteModel();
            $model->save([
                'id' => $this->request->getPost('id'),
                'from_to' => $this->request->getPost('from_to'),
                'award_in_points' => $this->request->getPost('award_in_points'),
                'cost_in_points' => $this->request->getPost('cost_in_points'),
            ]);
            session()->setFlashdata('message', lang('Airline.route_update_success'));

            return redirect()->to('/route');
        }
        else
        {
            return redirect()->to('/route/edit/'.$this->request->getPost('id'))->withInput();
        }
    }

    public function delete($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new RouteModel();
        $model->delete($id);
        return redirect()->to('/route');
    }
}