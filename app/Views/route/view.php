<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container main">
        <?php use CodeIgniter\I18n\Time; ?>
        <?php if (!empty($route)) : ?>
            <div class="card mb-3" style="max-width: 540px;">
                <div class="row">
                    <div class="col-md-4 d-flex align-items-center">
                        <img height="150" src="https://www.flaticon.com/svg/static/icons/svg/1983/1983844.svg" class="card-img" alt="<?= esc($route['from_to']); ?>">
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title">Маршрут № <?= esc($route['id']); ?></h5>
                            <div class="d-flex justify-content-between">
                                <div class="my-0">Наименование:</div>
                                <div class="text-muted"><?= esc($route['from_to']); ?></div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div class="my-0">Премия в баллах:</div>
                                <span class="badge badge-pill badge-primary"><?= esc($route['award_in_points']); ?></span>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div class="my-0">Стоимость в баллах:</div>
                                <span class="badge badge-pill badge-secondary"><?= esc($route['cost_in_points']); ?></span>
                            </div>
                            <a href="<?= base_url()?>/index.php/route/edit/<?= esc($route['id']); ?>" class="btn btn-primary">Редактировать</a>
                            <a href="<?= base_url()?>/index.php/route/delete/<?= esc($route['id']); ?>" class="btn btn-danger">Удалить</a>
                        </div>
                    </div>
                </div>
            </div>
        <?php else : ?>
            <p>Маршрут не найден.</p>
        <?php endif ?>
    </div>
<?= $this->endSection() ?>