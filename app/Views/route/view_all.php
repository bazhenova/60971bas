<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container main">
        <h2>Все маршруты</h2>

        <?php if (!empty($route) && is_array($route)) : ?>

            <?php foreach ($route as $item): ?>

                <div class="card mb-3" style="max-width: 540px;">
                    <div class="row">
                        <div class="col-md-4 d-flex align-items-center">
                            <img height="150" src="https://www.flaticon.com/svg/static/icons/svg/1983/1983844.svg" class="card-img" alt="<?= esc($item['from_to']); ?>">
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <h5 class="card-title">Маршрут № <?= esc($item['id']); ?></h5>
                                <p class="card-text"><?= esc($item['from_to']); ?></p>
                                <a href="<?= base_url()?>/index.php/route/view/<?= esc($item['id']); ?>" class="btn btn-primary">Подробнее</a>
                            </div>
                        </div>
                    </div>
                </div>

            <?php endforeach; ?>
        <?php else : ?>
            <p>Невозможно найти маршруты.</p>
        <?php endif ?>
    </div>
<?= $this->endSection() ?>