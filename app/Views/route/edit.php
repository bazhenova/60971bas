<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>

    <div class="container" style="max-width: 540px;">

        <?= form_open_multipart('route/update'); ?>
        <input type="hidden" name="id" value="<?= $route["id"] ?>">

        <div class="form-group">
            <label for="name">Наименование</label>
            <input type="text" class="form-control <?= ($validation->hasError('from_to')) ? 'is-invalid' : ''; ?>" name="from_to"
                   placeholder="Город отправления - Город назначения" value="<?= $route["from_to"]; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('from_to') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="name">Премия в баллах</label>
            <input type="text" class="form-control <?= ($validation->hasError('award_in_points')) ? 'is-invalid' : ''; ?>" name="award_in_points"
                   value="<?= $route["award_in_points"] ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('award_in_points') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="name">Стоимость в баллах</label>
            <input type="text" class="form-control <?= ($validation->hasError('cost_in_points')) ? 'is-invalid' : ''; ?>" name="cost_in_points"
                   value="<?= $route["cost_in_points"] ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('cost_in_points') ?>
            </div>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary" name="submit">Сохранить</button>
        </div>
        </form>
    </div>
<?= $this->endSection() ?>