<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="jumbotron text-center">
    <img class="mb-4" src="https://www.flaticon.com/svg/static/icons/svg/4052/4052188.svg" alt="" width="72" height="72"><h1 class="display-4">Airline Info</h1>
    <p class="lead">Это приложение поможет узнать информацию о маршрутах авиакомпании.</p>

    <?php if (! $ionAuth->loggedIn()): ?>
        <a class="btn btn-primary btn-lg" href="<?= base_url()?>/auth/login">Вход</a>
    <?php endif ?>
</div>
<?= $this->endSection() ?>
