<?php namespace App\Models;
use CodeIgniter\Model;
class RouteModel extends Model
{
    protected $table = 'route'; //таблица, связанная с моделью
    //Перечень задействованных в модели полей таблицы
    protected $allowedFields = ['from_to', 'award_in_points', 'cost_in_points'];
    public function getRoute($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }
}
